<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <title><?php echo $page_title ;?></title>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta content="telephone=no" name="format-detection"/>
    <meta name="HandheldFriendly" content="true"/>
    <link rel="stylesheet" href="assets/css/master.css"/>
    <link rel="stylesheet" href="assets/wowslider/style.css"/>
	
    <!-- SWITCHER-->
    <link href="assets/plugins/switcher/css/switcher.css" rel="stylesheet" id="switcher-css"/>
    <link href="assets/plugins/switcher/css/color1.css" rel="alternate stylesheet" title="color1"/>
    <link href="assets/plugins/switcher/css/color2.css" rel="alternate stylesheet" title="color2"/>
    <link href="assets/plugins/switcher/css/color3.css" rel="alternate stylesheet" title="color3"/>
    <link href="assets/plugins/switcher/css/color4.css" rel="alternate stylesheet" title="color4"/>
    <link href="assets/plugins/switcher/css/color5.css" rel="alternate stylesheet" title="color5"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico"/><!--[if lt IE 9 ]>
    <script src="/assets/js/separate-js/html5shiv-3.7.2.min.js" type="text/javascript"></script><meta content="no" http-equiv="imagetoolbar">
    <![endif]-->
    <script src="assets/libs/jquery-1.12.4.min.js"></script>
    <script src="assets/libs/typeahead.min.js"></script>
    <script src="assets/js/tableHeadFixer.js"></script>
  

    <style type="text/css">
    #dropdownMenu1 { background: none; border: none; margin-left: 10px; text-transform: uppercase; color: #222; font-family: Poppins; font-size: 13px; font-weight: 600; }
    .cRank ul li a { text-transform: uppercase; color: #222 !important; font-family: Poppins; font-size: 11px; font-weight: 600; }
    .navbar { padding-top: 0px; }
	
	/* Navigation
---------------------------------*/
.main-nav-outer{
	padding:0px;
	position:relative;
	float:right;
}
.main-nav{
	text-align:center;
	margin:10px 0 0px;
	padding:0;
	list-style:none;
}
.main-nav li{
	display:inline;
	margin:0 1px;
}
.main-nav li a{
	display:inline-block;
	color:#222222;
	text-transform:uppercase;
	font-family: 'Montserrat', sans-serif;
	text-decoration: none;
	line-height:20px;
	/* margin:17px 32px; */
	margin:6px 11px;
	transition:all 0.3s ease-in-out;
	-moz-transition:all 0.3s ease-in-out;
	-webkit-transition:all 0.3s ease-in-out;
}

.main-nav li a:hover{ 
	text-decoration:none;
	color: #EC3E02;
}

.small-logo{ 
	padding:0 32px;
}

.main-section{
	padding:90px 0 30px;
}

	
  </style>
 
  </head>
  
  <body>
    <!-- Loader-->
    <div id="page-preloader"><span class="spinner border-t_second_b border-t_prim_a"></span></div>
    <!-- Loader end-->
    
    <div data-header="sticky" data-header-top="200" data-canvas="container" class="l-theme animated-css">
      <!-- Start Switcher-->
      <!--<div class="switcher-wrapper">
        <div class="demo_changer">
          <div class="demo-icon text-primary"><i class="fa fa-cog fa-spin fa-2x"></i></div>
          <div class="form_holder">
            <div class="predefined_styles">
              <div class="skin-theme-switcher">
                <h4>Color</h4><a href="javascript:void(0);" data-switchcolor="color1" style="background-color:#fe3e01;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color2" style="background-color:#FFAC3A;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color3" style="background-color:#28af0f;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color4" style="background-color:#e425e9;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color5" style="background-color:#0c02bd;" class="styleswitch"></a>
              </div>
            </div>
          </div>
        </div>
    </div>-->
    <!-- end switcher-->
    <!-- ==========================-->
    <!-- SEARCH MODAL-->
    <!-- ==========================-->
    <div class="header-search open-search">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                    <div class="navbar-search">
                        <form class="search-global">
                            <input type="text" placeholder="Type to search" autocomplete="off" name="s" value="" class="search-global__input"/>
                            <button class="search-global__btn"><i class="icon stroke icon-Search"></i></button>
                            <div class="search-global__note">Begin typing your search above and press return to search.</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="search-close close"><i class="fa fa-times"></i></button>
    </div>
   

	
   
   
   
   
    <!-- ==========================-->
      <!-- FULL SCREEN MENU-->
      <!-- ==========================-->   
   
    <header class="header header-boxed-width header-background-trans header-logo-black header-topbarbox-1-left header-topbarbox-2-right header-navibox-1-left header-navibox-2-right header-navibox-3-right header-navibox-4-right">
        <div class="top-bar">
            <div class="container container-boxed-width">
                <div class="container">
              <div class="header-topbarbox-1">
                <ul class="top-bar-contact">
                  <li class="top-bar-contact__item"><i class="icon icon-call-in"></i> +91 8584979929</li>
                  <li class="top-bar-contact__item"><i class="icon icon-envelope-open"></i> info@bengalwatersports.in</li>
                  <!--<li class="top-bar-contact__item"><i class="icon icon-clock"></i> Mon – Fri  9.00 am – 6.00 pm</li>-->
                </ul>
              </div>
              <div class="header-topbarbox-2">
                <ul class="social-net list-inline">
                   <!--<li class="social-net__item"><a href="twitter.com" class="social-net__link text-primary_h"><i class="icon fa fa-twitter"></i></a></li>-->
                  <li class="social-net__item"><a href="https://www.facebook.com/bengalwatersports/" target="_blank" class="social-net__link text-primary_h"><i class="icon fa fa-facebook"></i></a></li>
                  <!--<li class="social-net__item"><a href="plus.google.com" class="social-net__link text-primary_h"><i class="icon fa fa-google-plus"></i></a></li>
                  <li class="social-net__item"><a href="linkedin.com" class="social-net__link text-primary_h"><i class="icon fa fa-linkedin"></i></a></li>-->
                </ul>
                <!-- end social-list-->
              </div>
                </div>
            </div>
        </div>
        <div class="container container-boxed-width">
            <nav id="nav" class="navbar">
                <div class="container">
                    <div class="header-navibox-1">
                        <!-- Mobile Trigger Start-->
                        <!-- <button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button> -->
                        <!-- Mobile Trigger End-->
                        <a href="index" class="navbar-brand scroll"><img src="assets/media/general/logo.png" alt="logo" class="normal-logo"/><img src="assets/media/general/logo-dark.png" alt="logo" class="scroll-logo hidden-xs logo_my"/></a>
                    </div>
					  
                   <div class="header-navibox-2">
                      <nav class="main-nav-outer" id="test"><!--main-nav-start-->
						<div class="container">
							<ul class="main-nav">
								<li><a href="index">Home</a></li>
								<li><a href="about">About</a></li>
								<li><a href="gallery">Gallery</a></li>
								 <li class="dropdown">
									<a href="#" data-toggle="dropdown" class="dropdown-toggle">Championship Rank<b class="caret"></b>
									</a>
									<ul class="dropdown-menu">
										<li><a href="intraschool">Asian Intra School</a></li>
										<li><a href="interschool">Asian Inter School</a></li>
										<li><a href="asian-indoor">Asian School Grand Finale</a></li>
									</ul>
								</li>
								<li><a href="players">Players</a></li>
								<li><a href="contact">Contact</a></li>
							</ul>
							<a class="res-nav_click" href="#"><i class="fa fa-bars" style="font-size:20px;"></i></a>
						</div>
					</nav><!--main-nav-end-->
                    </div>
                </div>
            </nav>
        </div>
    </header>
	
    <!-- end .header-->